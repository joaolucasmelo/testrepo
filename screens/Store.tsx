import React, {useState} from 'react';
import {FlatList, SafeAreaView, ScrollView, StyleSheet} from 'react-native';
import {getProducts} from '../components/store/client';
import Product from '../components/store/Product';
import {useNavigation} from '@react-navigation/native';
import LiveChat from 'react-native-livechat';

const Store = () => {
  let [products, setProducts] = useState<
    Array<{
      id: Number;
      title: String;
      price: Number;
      description: String;
      category: String;
      image: String;
      rating: {rate: Number; count: Number};
    }>
  >();

  const navigation = useNavigation();

  const handleGet = async () => {
    const productsApi = await getProducts();
    setProducts((products = productsApi.data));
  };
  handleGet();
  return (
    <SafeAreaView>
      <ScrollView style={styles.containter}>
        <FlatList
          data={products}
          numColumns={2}
          horizontal={false}
          renderItem={({item}) => (
            <Product
              product={item}
              onPress={() =>
                navigation.navigate(
                  'Product Details' as never,
                  {product: item} as never
                )
              }
            />
          )}
        />
      </ScrollView>
            <LiveChat
        // bubbleStyles={styles.bubbleStyles}
        bubbleColor="dodgerblue"
        license="13537692"
        redirectUri="https://direct.lc.chat/13537692/"
        clientId="e6585acd8cadf6488ef2ec4ad5e2102a"
        textInputStyle={{color: 'black'}}
      />
    </SafeAreaView>
  );
};
const styles = StyleSheet.create({
  containter: {}
});
export default Store;
