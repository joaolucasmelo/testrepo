import React from 'react';
import {StyleSheet, View} from 'react-native';
import LiveChat from 'react-native-livechat';
import {WebView} from 'react-native-webview';

const LiveChatScreen = () => {
  console.warn = () => {};

  return (
    <View style={{height: '100%'}}>
      <WebView
        source={{
          uri: 'https://secure.livechatinc.com/licence/13537692/v2/open_chat.cgi',
        }}
        />
      {/* <LiveChat
        bubbleStyles={styles.bubbleStyles}
        bubbleColor="dodgerblue"
        license="13537692"
        redirectUri="https://secure.livechatinc.com/licence/13537692/v2/open_chat.cgi"
        clientId="e6585acd8cadf6488ef2ec4ad5e2102a"
        textInputStyle={{color: 'black'}}
      /> */}
    </View>
  );
};

const styles = StyleSheet.create({
  bubbleStyles: {
    position: 'absolute',
    right: 20,
    bottom: 20,
  },
});
export default LiveChatScreen;
