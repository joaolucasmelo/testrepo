import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import Tab from './components/Tab';
import LiveChat from 'react-native-livechat';
import { StyleSheet } from 'react-native';

const App = () => {
  console.warn = () => {};

  return (
    <NavigationContainer>
      <Tab />
      {/* <LiveChat
        bubbleStyles={styles.bubbleStyles}
        bubbleColor="dodgerblue"
        license="13537692"
        redirectUri="https://secure.livechatinc.com/licence/13537692/v2/open_chat.cgi"
        clientId="e6585acd8cadf6488ef2ec4ad5e2102a"
        textInputStyle={{color: 'black'}}
      /> */}
      </NavigationContainer>
  );
};
const styles = StyleSheet.create({
  bubbleStyles: {
    position: 'absolute',
    right: 15,
    bottom: 65,
  },
});
export default App;
