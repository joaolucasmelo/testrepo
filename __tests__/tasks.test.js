import React from 'react';
import renderer from 'react-test-renderer';
import CreateNote from '../components/tasks/CreateNote';

test('renders correctly', () => {
  const tree = renderer.create(<CreateNote />).toJSON();
  expect(tree).toMatchSnapshot();
});