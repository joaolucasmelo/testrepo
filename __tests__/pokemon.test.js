/**
 * @jest-environment jsdom
 */

import React from 'react';
import renderer, { act }  from 'react-test-renderer';
import PageTwo from '../screens/PageTwo';
import { fireEvent, waitFor, render } from '@testing-library/react-native'

// test('renders correctly', () => {
//   const tree = renderer.create(<PageTwo />).toJSON();
//   expect(tree).toMatchSnapshot();
// });

describe("Check Input Pokemon", () => {
  test('renders correctly', async () => {
    renderer.create(<PageTwo />).toJSON();
    const { getByTestId, getByText, queryByTestId } = render(<PageTwo />)
    const pokemon = 'pikachu';

    const input = queryByTestId('input');
    fireEvent.changeText(input, pokemon)


    const button = getByText('Find Pokemon')
    fireEvent.press(button)

    await waitFor(() => expect(queryByTestId('printed-pokemon').props.children).toBeTruthy())

    await act(async () => expect(getByTestId('printed-pokemon').props.children).toBe(pokemon));
  })
});
